require 'pry'
require 'stock_quote'
require 'eu_central_bank'

input = ARGV

from = 'USD'
to = 'KRW'

#bank = EuCentralBank.new
#bank.update_rates

DATA.each_line do |c|
    c.chomp!
    stock = StockQuote::Stock.quote(c)
    price = stock.last_trade_price_only
    bank = EuCentralBank.new
    bank.update_rates
    result = bank.exchange(("#{price}".ord)*100,from,to)
    puts "#{stock.name} is $#{stock.last_trade_price_only}"    
    puts "#{from} => #{to} : #{price}$ => #{result}￦"
end

__END__
a
b
c