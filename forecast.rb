require 'pry'
require 'forecast_io'

ForecastIO.configure do |c|
    c.api_key = '3a475294924874cda9b1036f5150160a'
end
\
#binding.pry
forecast = ForecastIO.forecast(37.566535, 126.9779692)
f = forecast.currently

puts f.summary
puts ((f.apparentTemperature - 32) * 5 / 9).round(1)
# 37.566535
# 126.9779692